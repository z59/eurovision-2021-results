#!/bin/sh

while IFS= read -r C; do
	printf %s\\n "$C"
	mkdir -p "data/t/$C" "data/j/$C"
	wget -qO- "https://eurovision.tv/event/rotterdam-2021/grand-final/results/$C" |
	tr -d \\n | sed -r 's#.*Points given by televoters|Detailed voting breakdown.*##g;s#Points given by the jury#\n#' |
	{
		IFS= read -r L
		printf %s\\n "$L" | sed -r 's#</tr> *<tr class="text-base">#\n#g;s# *<[^i][^>]*> *##g;s# *<[^>]*> *# #g' |
		while IFS=' ' read -r P C2; do
			printf %s\\n "$P" >"data/t/$C/$C2.txt"
		done
		IFS= read -r L
		printf %s\\n "$L" | sed -r 's#</tr> *<tr class="text-base">#\n#g;s# *<[^i][^>]*> *##g;s# *<[^>]*> *# #g' |
		while IFS=' ' read -r P C2; do
			printf %s\\n "$P" >"data/j/$C/$C2.txt"
		done
	}
done <c-list.txt

