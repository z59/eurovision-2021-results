#!/bin/sh

while IFS= read -r C; do
	TOTAL=0
	while IFS=' ' read -r P C2; do
		P_J=$(cat "data/j/$C/$C2.txt" 2>/dev/null || echo 0)
		DIFF=$(( $P_J*38 - $P ))
		TOTAL=$(( $TOTAL + ${DIFF#-} ))
	done <received-t.txt
	printf %s\\n "$TOTAL $C"
done <c-list.txt | sort -n | tee local-jury-vs-global-televoters.txt

