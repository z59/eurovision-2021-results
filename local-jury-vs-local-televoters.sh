#!/bin/sh

while IFS= read -r C; do
	TOTAL=0
	while IFS= read -r C2; do
		P_J=$(cat "data/j/$C/$C2.txt" 2>/dev/null || echo 0)
		P_T=$(cat "data/t/$C/$C2.txt" 2>/dev/null || echo 0)
		DIFF=$(( $P_J - $P_T ))
		TOTAL=$(( $TOTAL + ${DIFF#-} ))
	done <c-list-final-with-votes.txt
	printf %s\\n "$TOTAL $C"
done <c-list.txt | sort -n | tee local-jury-vs-local-televoters.txt

