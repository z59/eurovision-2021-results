#!/bin/sh

>received-t.txt
>received-j.txt
>received.txt

while IFS= read -r C; do
	printf %s\\n "$C"
	P_T=$( find data/t/ -type f -name "$C.txt" -exec cat {} + | tr \\n + )
	P_T=${P_T%+}
	P_T=$(( $P_T ))
	printf %s\\n "$P_T $C" >>received-t.txt
	P_J=$( find data/j/ -type f -name "$C.txt" -exec cat {} + | tr \\n + )
	P_J=${P_J%+}
	P_J=$(( $P_J ))
	printf %s\\n "$P_J $C" >>received-j.txt
	printf %s\\n "$(( $P_T + $P_J )) $C" >>received.txt
done <c-list-final-with-votes.txt

